# 简要
Loogn.OrmLite是一个简单、高效的SqlServer数据库访问组件！
前段时间写过[Loog.DataAccess](http://git.oschina.net/loogn/Loogn.DataAccess)，以为不会再在这方面下功夫了!
但是发现这是个永无止境的过程，总结了上个项目的不足，参考了ServiceStack.OrmLite的API，重写了一个Loogn.OrmLite


# API

[http://www.loogn.net/blog/article.aspx?id=44](http://www.loogn.net/blog/article.aspx?id=44)